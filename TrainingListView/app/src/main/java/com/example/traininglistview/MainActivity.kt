package com.example.traininglistview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import com.example.traininglistview.adapter.ListViewAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var listView: ListView
    private lateinit var list : ArrayList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        setupListView()


    }

    private fun setupListView() {
        listView.adapter = ListViewAdapter(this,R.layout.row_item,list)
    }

    private fun init() {
        listView = findViewById(R.id.lv)
        list = arrayListOf("Paris","London","NewYork","Tokyo","Ha Noi","Manchester",
            "Liverpool","Kyoto","Madrid","Rome","Paris","London","NewYork","Tokyo","Ha Noi","Manchester",
            "Paris","London","NewYork","Tokyo","Ha Noi","Manchester","Paris","London","NewYork","Tokyo","Ha Noi","Manchester")
    }
}