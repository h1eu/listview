package com.example.traininglistview.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import com.example.traininglistview.R

class ListViewAdapter(var mContext : Context,var rs : Int,var listString : ArrayList<String>) :
    ArrayAdapter<String>(mContext, rs,listString) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        Log.e("view","view o vi tri: " + position.toString())
        val rowItem : View = LayoutInflater.from(mContext).inflate(rs,parent,false)
        val tv : TextView = rowItem.findViewById(R.id.tv)
        tv.text = listString.get(position)
        return rowItem
    }

}